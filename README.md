# MEAN Starter 1.0.0

The purpose of this project is to give me (or you!) a MEAN stack base structure which I can immediately build on.

## Technologies Involved

* MEAN Stack:
  * MongoDB + Mongoose
  * Express
  * Angular (v4)
  * Node.js
* bcrypt.js
* JSON Web Tokens

### Tools

* Angular CLI
* Heroku and mLab for deployment

## Start

1. `npm install`
1. Start `mongod`
1. `node server` (in a terminal)
1. `npm run watch` (in another terminal)

### Deployment

1. Make the adequate replacements in `src/environments/environment.prod.ts:3`* and `server.js:9`.
1. Also in: `server/routes/user.js:12`, `server/routes/user.js:49`, and `server/routes/message.js:27`.
1. `ng build --prod`
1. `git add . && git add -f dist/`
1. `git commit -m "Commit Message"`
1. `git push [-f] heroku [current-branch:]master`
1. Might be useful: `heroku logs --tail`.

*Be careful not to leave a trailing slash in the address!

## Resources Used

* [Setting up an Angular 4 MEAN Stack (Tutorial)](https://coursetro.com/posts/code/84/Setting-up-an-Angular-4-MEAN-Stack-(Tutorial))
* [Angular (Angular 2+) & NodeJS - The MEAN Stack Guide](https://www.udemy.com/angular-2-and-nodejs-the-practical-guide/)

## Caveat

* "On a real server, you might really think about using SSL to encrypt the data you send over your network!" (Maximilian Schwarzmüller, in _Angular (Angular 2+) & NodeJS - The MEAN Stack Guide_)

## Possible Improvements

* Add unit tests.
* Hide "Sign Up" page when the user is signed in (this may implies routing, since the Sign Up page is the default child route of the User route).
* In `MessageInputComponent.onSave` method, pass the user's first name as username rather than a hard coded string.

---
---
---

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.4.8.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
