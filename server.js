const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const http = require('http');
const mongoose = require('mongoose');

const app = express();

// Prod: 'mongodb://<dbuser>:<dbpassword>@<host>:<port>/<project-name>'
mongoose.connect('mongodb://localhost:27017/mean-starter', {
  useMongoClient: true
});

// API files for interacting with MongoDB
const messageApi = require('./server/routes/message');
const userApi = require('./server/routes/user');

// Parsers
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// Angular DIST output folder
app.use(express.static(path.join(__dirname, 'dist')));

// APIs location
app.use('/api/message', messageApi);
app.use('/api/user', userApi);

// Send all other requests to the Angular app
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist/index.html'));
});

//Set Port
const port = process.env.PORT || '3000';
app.set('port', port);

const server = http.createServer(app);

server.listen(port, () => console.log(`Running on localhost:${port}`));
