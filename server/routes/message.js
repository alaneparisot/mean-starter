const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');

const Message = require('../models/message');
const User = require('../models/user');

router.get('/', function (req, res) {
  Message.find()
    .populate('user', 'firstName')
    .exec(function (err, messages) {
      if (err) {
        return res.status(500).json({
          title: 'Something went wrong while getting messages...',
          error: err
        });
      }
      res.status(200).json({
        message: 'Messages were successfully fetched!',
        obj: messages
      });
    });
});

// Route Protection
router.use('/', function (req, res, next) {
  jwt.verify(req.query.token, 'secret', function (err, decoded) {
    if (err) {
      return res.status(401).json({
        title: 'Authentication is required to execute this action!',
        error: err
      });
    }
    next();
  });
});

router.delete('/:id', function (req, res) {
  const decoded = jwt.decode(req.query.token);
  Message.findById(req.params.id, function (err, message) {
    if (err) {
      return res.status(500).json({
        title: 'Something went wrong while looking for message...',
        error: err
      });
    }
    if (!message) {
      return res.status(500).json({
        title: 'Message was not found!',
        error: {message: `Message identified by ID ${req.params.id} was not found.`}
      });
    }
    if (message.user != decoded.user._id) {
      return res.status(401).json({
        title: 'Users don\'t match!',
        error: {message: 'A user can only delete his/her own message(s).'}
      });
    }
    message.remove(function (err, result) {
      if (err) {
        return res.status(500).json({
          title: 'Something went wrong while deleting message...',
          error: err
        });
      }
      res.status(200).json({
        message: 'Message was successfully deleted!',
        obj: result
      });
    });
  });
});

router.patch('/:id', function (req, res) {
  const decoded = jwt.decode(req.query.token);
  Message.findById(req.params.id, function (err, message) {
    if (err) {
      return res.status(500).json({
        title: 'Something went wrong while looking for message...',
        error: err
      });
    }
    if (!message) {
      return res.status(500).json({
        title: 'Message was not found!',
        error: {message: `Message identified by ID ${req.params.id} was not found.`}
      });
    }
    if (message.user != decoded.user._id) {
      return res.status(401).json({
        title: 'Users don\'t match!',
        error: {message: 'A user can only edit his/her own message(s).'}
      });
    }
    message.content = req.body.content;
    message.save(function (err, result) {
      if (err) {
        return res.status(500).json({
          title: 'Something went wrong while updating message...',
          error: err
        });
      }
      res.status(200).json({
        message: 'Message was successfully updated!',
        obj: result
      });
    });
  });
});

router.post('/', function (req, res) {
  const decoded = jwt.decode(req.query.token);
  User.findById(decoded.user._id, function (err, user) {
    if (err) {
      return res.status(500).json({
        title: 'Something went wrong while saving message...',
        error: err
      });
    }
    const message = new Message({
      content: req.body.content,
      user: user._id
    });
    message.save(function (err, result) {
      if (err) {
        return res.status(500).json({
          title: 'Something went wrong while saving message...',
          error: err
        });
      }
      user.messages.push(result);
      user.save();
      result.user = user;
      res.status(201).json({
        message: 'Message was successfully saved!',
        obj: result
      });
    });
  });
});

module.exports = router;
