const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const User = require('../models/user');

router.post('/sign-up', function (req, res) {
  const user = new User({
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    password: bcrypt.hashSync(req.body.password, 10),
    email: req.body.email,
  });
  user.save(function (err, result) {
    if (err) {
      return res.status(500).json({
        title: 'Something went wrong while saving user...',
        error: err
      });
    }
    res.status(201).json({
      message: 'User was successfully saved!',
      obj: result
    });
  });
});

router.post('/sign-in', function (req, res) {
  User.findOne({email: req.body.email}, function (err, user) {
    if (err) {
      return res.status(500).json({
        title: 'Something went wrong while signing in...',
        error: err
      });
    }
    if (!user) {
      return res.status(401).json({
        title: 'Something went wrong while signing in...',
        error: {message: 'E-mail address or password or both are incorrect.'}
      });
    }
    if (!bcrypt.compareSync(req.body.password, user.password)) {
      return res.status(401).json({
        title: 'Something went wrong while signing in...',
        error: {message: 'E-mail address or password or both are incorrect.'}
      });
    }
    const token = jwt.sign({user: user}, 'secret', {expiresIn: 7200});
    res.status(200).json({
      message: 'Successfully signed in!',
      token: token,
      userId: user._id
    });
  });
});

module.exports = router;
