import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MessagesComponent } from './messages/messages.component';
import { AuthComponent } from './auth/auth.component';

const routes: Routes = [
  {path: '', redirectTo: '/messages', pathMatch: 'full'},
  {path: 'messages', component: MessagesComponent},
  {path: 'auth', component: AuthComponent, loadChildren: './auth/auth.module#AuthModule'},
  {path: '**', redirectTo: '/messages'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
