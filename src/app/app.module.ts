import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

import { AppRoutingModule } from './app-routing.module';
import { MessagesModule } from './messages/messages.module';

import { AppComponent } from './app.component';
import { HeaderComponent } from './shared/header/header.component';
import { AuthComponent } from './auth/auth.component';
import { AuthService } from './auth/service/auth.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    AuthComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    AppRoutingModule,
    MessagesModule
  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule {}
