import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { authRouting } from './auth.routes';

import { SignUpComponent } from './sign-up/sign-up.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignOutComponent } from './sign-out/sign-out.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    authRouting
  ],
  declarations: [
    SignUpComponent,
    SignInComponent,
    SignOutComponent
  ]
})
export class AuthModule {}
