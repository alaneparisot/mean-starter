import { RouterModule, Routes } from '@angular/router';

import { SignUpComponent } from './sign-up/sign-up.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignOutComponent } from './sign-out/sign-out.component';

const authRoutes: Routes = [
  {path: '', redirectTo: 'sign-up', pathMatch: 'full'},
  {path: 'sign-up', component: SignUpComponent},
  {path: 'sign-in', component: SignInComponent},
  {path: 'sign-out', component: SignOutComponent},
  {path: '**', redirectTo: 'sign-up'}
];

export const authRouting = RouterModule.forChild(authRoutes);
