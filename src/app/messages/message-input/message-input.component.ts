import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { Message } from '../model/message.model';
import { MessageService } from '../service/message.service';

@Component({
  selector: 'app-message-input',
  templateUrl: './message-input.component.html',
  styleUrls: ['./message-input.component.scss']
})
export class MessageInputComponent implements OnInit {
  message: Message;

  constructor(private messageService: MessageService) { }

  ngOnInit() {
    this.messageService.messageIsEdited
      .subscribe(
        (message: Message) => this.message = message
      );
  }

  onClear(form: NgForm) {
    this.message = null;
    form.resetForm();
  }

  onSave(form: NgForm) {
    if (this.message) {
      // Edition
      this.message.content = form.value.content;
      this.messageService.updateMessage(this.message)
        .subscribe(
          result => console.log(result),
          error => console.error(error)
        );
      this.message = null;
    } else {
      // Creation
      const message = new Message(form.value.content, 'Alane');
      this.messageService.addMessage(message)
        .subscribe(
          data => console.log(data),
          error => console.error(error)
        );
    }

    form.resetForm();
  }

}
