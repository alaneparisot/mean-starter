import { Component, Input, OnInit } from '@angular/core';

import { Message } from '../model/message.model';
import { MessageService } from '../service/message.service';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit {
  @Input() message: Message;

  constructor(private messageService: MessageService) { }

  ngOnInit() { }

  belongsToUser() {
    return localStorage.getItem('userId') === this.message.userId;
  }

  onDelete() {
    this.messageService.deleteMessage(this.message)
      .subscribe(
        result => console.log(result),
        error => console.error(error)
      );
  }

  onEdit() {
    this.messageService.editMessage(this.message);
  }

}
