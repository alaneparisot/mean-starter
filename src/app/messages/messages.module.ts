import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { MessagesComponent } from './messages.component';
import { MessageComponent } from './message/message.component';
import { MessageInputComponent } from './message-input/message-input.component';
import { MessageListComponent } from './message-list/message-list.component';
import { MessageService } from './service/message.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [
    MessagesComponent,
    MessageComponent,
    MessageListComponent,
    MessageInputComponent,
  ],
  providers: [MessageService]
})
export class MessagesModule { }
