import { EventEmitter, Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { environment } from '../../../environments/environment';

import { Message } from '../model/message.model';

@Injectable()
export class MessageService {
  public messageIsEdited = new EventEmitter<Message>();

  private messages: Message[] = [];
  private messageApiUrl = environment.host + '/api/message';

  constructor(private http: Http) { }

  addMessage(message: Message) {
    const body = JSON.stringify(message);
    const headers = new Headers({'Content-Type': 'application/json'});
    const token = localStorage.getItem('token')
      ? `?token=${localStorage.getItem('token')}`
      : '';
    return this.http.post(this.messageApiUrl + token, body, {headers: headers})
      .map((response: Response) => {
        const result = response.json();
        const createdMessage = new Message(
          result.obj.content,
          result.obj.user.firstName,
          result.obj._id,
          result.obj.user._id);
        this.messages.push(createdMessage);
        return createdMessage;
      })
      .catch((error: Response) => Observable.throw(error.json()));
  }

  deleteMessage(message: Message) {
    this.messages.splice(this.messages.indexOf(message), 1);
    const token = localStorage.getItem('token')
      ? `?token=${localStorage.getItem('token')}`
      : '';
    return this.http.delete(this.messageApiUrl + '/' + message.messageId + token)
      .map((response: Response) => response.json())
      .catch((error: Response) => Observable.throw(error.json()));
  }

  editMessage(message: Message) {
    this.messageIsEdited.emit(message);
  }

  getMessages() {
    return this.http.get(this.messageApiUrl)
      .map((response: Response) => {
        const fetchedMessages = response.json().obj;
        const formattedMessages: Message[] = [];
        for (const message of fetchedMessages) {
          formattedMessages.push(new Message(
            message.content,
            message.user.firstName,
            message._id,
            message.user._id)
          );
        }
        this.messages = formattedMessages;
        return formattedMessages;
      })
      .catch((error: Response) => Observable.throw(error.json()));
  }

  updateMessage(message: Message) {
    const body = JSON.stringify(message);
    const headers = new Headers({'Content-Type': 'application/json'});
    const token = localStorage.getItem('token')
      ? `?token=${localStorage.getItem('token')}`
      : '';
    return this.http.patch(this.messageApiUrl + '/' + message.messageId + token, body, {headers: headers})
      .map((response: Response) => response.json())
      .catch((error: Response) => Observable.throw(error.json()));
  }

}
